
var inHTML1,
	inHTML2,
	inHTML3;

$(document).keypress(function(e) {
	
    if(e.which == 13) {
        searchForm();
    }
    /*awesomplete.close()*/
});

function searchForm(){
    var input = document.getElementById("txtSearcher");
    var awesomplete  = new Awesomplete(input, listSearch);
    var people = listAll,
        libAll = people.slice(0, 10),
        libfirts = libAll.slice(0, 3),
        libSecond = libAll.slice(3, 6),
        libThird = libAll.slice(6, 9);

    $.each(libfirts, function(index, value){
    	var newItem = "<div class='data'>"  +
    					"<div class='title-data'>" +
    					 	"<div class='image'>" +
    					 		"<img src="+ value.image + ">" +
    					 	"</div>" + 
    					"</div>" +
    					"<div class='description'>" +
      						"<div class='title'>" + 
       			 				"<h4>" + value.title + "</h4>" +
      						"</div>"+
      						"<span>" + value.teaser + "</span>" +
    					"</div>" +
    				 "</div>";
    
        inHTML1 += newItem;  
	});

    $("#libfirts").html(inHTML1);


    $.each(libSecond, function(index, value){
    	var newItem = "<div class='data'>"  +
    					"<div class='title-data'>" +
    					 	"<div class='image'>" +
    					 		"<img src=" + value.image + ">" +
    					 	"</div>" + 
    					"</div>" +
    					"<div class='description'>" +
      						"<div class='title'>" + 
       			 				"<h4>" + value.title + "</h4>" +
      						"</div>"+
      						"<span>" + value.teaser + "</span>" +
    					"</div>" +
    				 "</div>";
    
        inHTML2 += newItem;  
	});

    $("#libSecond").html(inHTML2);


     $.each(libThird, function(index, value){
    	var newItem = "<div class='data'>"  +
    					"<div class='title-data'>" +
    					 	"<div class='image'>" +
    					 		"<img src="+ value.image + ">" +
    					 	"</div>" + 
    					"</div>" +
    					"<div class='description'>" +
      						"<div class='title'>" + 
       			 				"<h4>" + value.title + "</h4>" +
      						"</div>"+
      						"<span>" + value.teaser + "</span>" +
    					"</div>" +
    				 "</div>";
    
        inHTML3 += newItem;  
	});

    $("#libThird").html(inHTML3);

}

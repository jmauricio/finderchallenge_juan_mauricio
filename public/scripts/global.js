/*
  constants and global functions
*/

var JSON_FILE = '/books-schema.json',
    listSearch = {
        list:[]
    },
    listAll;

/*
 @method loadJSON
 source: https://codepen.io/KryptoniteDove/post/load-json-file-locally-using-pure-javascript
*/

function unique(a){
    var seen = [], result = [];
    for(var len = a.length, i = len-1; i >= 0; i--){
        if(!seen[a[i]]){
            seen[a[i]] = true;
            result.push(a[i]);
        }
    }
    return result;
}

function loadJSON(url, callback){
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", url, true);
    xobj.onreadystatechange = function(responseText){
       
        if(xobj.readyState == 4 && xobj.status == "200"){
            var content = JSON.parse(xobj.responseText);
            callback.call(this, content);
        }
    };
    xobj.send(null);
};

loadJSON(JSON_FILE, function(response){
    var actual_JSON = response,
        data = actual_JSON.data;
        listAll = data;
   
    data.forEach(function(data){
        listSearch.list.push(data.title);
    });

    unique(listSearch.list);
});


